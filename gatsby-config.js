module.exports = {
  pathPrefix: `/pages_gatsby`,
  siteMetadata: {
    title: `Gatsby Default Starter`,
  },
  plugins: [`gatsby-plugin-react-helmet`],
}
